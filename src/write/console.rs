use super::{Result, WriteBehavior};

pub struct Console;

impl WriteBehavior for Console {
    fn write(&self, message: String) -> Result<()> {
        println!("{}", message);
        Ok(())
    }
}

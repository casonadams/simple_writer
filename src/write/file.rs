use super::{Result, WriteBehavior};
use std::fs;

pub struct File;

impl WriteBehavior for File {
    fn write(&self, message: String) -> Result<()> {
        fs::write("output.txt", message.as_bytes())?;
        Ok(())
    }
}

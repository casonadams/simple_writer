use write::console::Console;
use write::file::File;
use write::Write;

mod write;

fn main() {
    let file_writer = Write::new(Box::new(File));
    file_writer.write("test".to_string()).ok();

    let console_writer = Write::new(Box::new(Console));
    console_writer.write("test".to_string()).ok();
}

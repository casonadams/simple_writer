use anyhow::Result;

pub mod console;
pub mod file;

pub trait WriteBehavior {
    fn write(&self, message: String) -> Result<()>;
}

pub struct Write {
    behaviour: Box<dyn WriteBehavior>,
}

impl Write {
    pub fn new(behaviour: Box<dyn WriteBehavior>) -> Self {
        Self { behaviour }
    }

    pub fn write(&self, message: String) -> Result<()> {
        self.behaviour.write(message)
    }
}
